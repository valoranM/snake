cmake_minimum_required(VERSION 3.0)

project(snake)

set(SRCS
    src/coord.c
    src/grille.c
    src/snake.c
)

set(HEADERS
    src/coord.h
    src/grille.h
    src/snake.h
)

# add_compile_options(-Wall -Wextra -Werror -pedantic -Werror)
add_executable(snake src/main.c ${SRCS} ${HEADERS})
target_compile_features(snake PRIVATE cxx_std_17)
target_include_directories(snake PUBLIC src/)
target_link_libraries(snake ncurses)
