# Snake

<h1 align="center">
  <br>
  <img src="screen/screen.png" alt="Poyuterm logo" width="256">
  <br>
</h1>

<h4 align="center">Small games in the terminal 🎮</h4>


I coded this little game during a lecture because I didn't really know what to do

## Get Snake
#### Required libs and tools:
- cmake
- pkg-config
- ncurses

#### Compilation
```sh
cmake .
make
```

#### Launching snake
```
./snake dimX dimY

```