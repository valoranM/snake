#ifndef GRILLE_H
#define GRILLE_H

#include "coord.h"

#include <stddef.h>
#include <ncurses.h>

enum Case
{
    None,
    Snake,
    SnakeH,
    Apple
};

typedef enum Case Case;

extern void init_grille(size_t w, size_t h);

extern Case get_case(size_t x, size_t y);

extern void set_case(size_t x, size_t y, Case c);

extern void set_case_dot(dot d, Case c);

extern void add_apple();

extern size_t get_width();

extern size_t get_height();

extern void remove_grille();

#endif /* GRILLE_H */