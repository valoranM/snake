#include "snake.h"
#include "grille.h"

#include <stdlib.h>
#include <time.h>

static snake_h snake;

static void remove_snake_c();
static int input(int code);
static snake_c *grow(int x, int y);
static void change_last_dir();

void init_snake(size_t size, size_t w, size_t h)
{
    int x = w / 2, y = h / 2;
    snake.dir.x = -1;
    snake.dir.y = 0;
    snake.c = NULL;
    snake.last_key = KEY_LEFT;
    snake.apple = 0;

    snake_c *prev = snake.c, *tmp_c;
    for (size_t i = 0; i < size; i++)
    {
        tmp_c = (snake_c *)malloc(sizeof(snake_c));
        tmp_c->next = NULL;
        tmp_c->prev = prev;
        change_coord(&(tmp_c->d), x + i, y);
        prev = tmp_c;
        prev->next = tmp_c;
        if (i == 0)
        {
            snake.c = tmp_c;
            set_case(tmp_c->d.x, tmp_c->d.y, SnakeH);
        }
        else
        {
            set_case(tmp_c->d.x, tmp_c->d.y, Snake);
        }
    }
    tmp_c->next = snake.c;
    snake.c->prev = tmp_c;
    mvprintw(0, get_width() + 4, "apple = %d", snake.apple);
}

int game(int w, int h)
{
    time_t t, last_frame = clock();
    unsigned long ms;
    int c;
    while (1)
    {
        t = clock();
        ms = (t - last_frame) * 1000 / CLOCKS_PER_SEC;

        if (input(getch()))
        {
            return 1;
        }

        if (ms < 200)
        {
            continue;
        }
        else
        {
            int x = snake.c->d.x + snake.dir.x;
            int y = snake.c->d.y + snake.dir.y;
            if (x < 0 || x >= w || y < 0 || y >= h ||
                get_case(x, y) == Snake || get_case(x, y) == SnakeH)
            {
                return 0;
            }
            else
            {
                if (get_case(x, y) == Apple)
                {
                    add_apple();

                    snake_c *tmp = grow(x, y);
                    tmp->next = snake.c;
                    tmp->prev = snake.c->prev;
                    tmp->prev->next = tmp;
                    tmp->next->prev = tmp;
                    snake.c = tmp;

                    set_case_dot(snake.c->next->d, Snake);
                    set_case(x, y, SnakeH);
                    snake.apple++;
                    mvprintw(0, get_width() + 4, "apple = %d", snake.apple);
                }
                else
                {
                    set_case_dot(snake.c->d, Snake);
                    snake.c = snake.c->prev;

                    set_case_dot(snake.c->d, None);
                    change_coord(&(snake.c->d), x, y);
                    set_case_dot(snake.c->d, SnakeH);
                }
            }
            change_last_dir();
            last_frame = clock();
        }
    }
    return 1;
}

static void remove_snake_c()
{
    snake_c *tmp, *s = snake.c;
    tmp = snake.c;
    if (tmp->next != tmp->next)
    {
        tmp->prev->next = tmp->next;
        tmp->next->prev = tmp->prev;
        snake.c = tmp->next;
    }
    else
    {
        snake.c = NULL;
    }
    free(tmp);
}

static int input(int code)
{
    switch (code)
    {
    case KEY_DOWN:
        if (snake.last_key != KEY_UP)
        {
            snake.dir.x = 0;
            snake.dir.y = 1;
        }
        return 0;

    case KEY_LEFT:
        if (snake.last_key != KEY_RIGHT)
        {
            snake.dir.x = -1;
            snake.dir.y = 0;
        }
        return 0;

    case KEY_UP:
        if (snake.last_key != KEY_DOWN)
        {
            snake.dir.x = 0;
            snake.dir.y = -1;
        }
        return 0;

    case KEY_RIGHT:
        if (snake.last_key != KEY_LEFT)
        {
            snake.dir.x = 1;
            snake.dir.y = 0;
        }
        return 0;

    case 'q':
        return 1;

    default:
        return 0;
    }
    return 0;
}

static snake_c *grow(int x, int y)
{
    snake_c *tmp = (snake_c *)malloc(sizeof(snake_c));
    tmp->d.x = x;
    tmp->d.y = y;

    return tmp;
}

void remove_snake()
{
    while (snake.c != NULL)
    {
        remove_snake_c();
    }
}

static void change_last_dir()
{
    if (snake.dir.x == 1)
    {
        snake.last_key = KEY_RIGHT;
    }
    else if (snake.dir.x == 0)
    {
        if (snake.dir.y == 1)
        {
            snake.last_key = KEY_DOWN;
        }
        else if (snake.dir.y == -1)
        {
            snake.last_key = KEY_UP;
        }
    }
    else if (snake.dir.x == -1)
    {
        snake.last_key = KEY_LEFT;
    }
}