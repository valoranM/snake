#include "coord.h"

#include <stdlib.h>

dot *new_coord(int x, int y)
{
    dot *tmp = (dot *)malloc(sizeof(dot));
    tmp->x = x;
    tmp->y = y;
    return tmp;
}

void change_coord(dot *p, int x, int y)
{
    p->x = x;
    p->y = y;
}