#ifndef DOT_H
#define DOT_H

typedef struct dot
{
    int x, y;
} dot;

extern dot *new_coord(int x, int y);

extern void change_coord(dot *p, int x, int y);

#endif /* DOT_H */