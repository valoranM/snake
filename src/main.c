#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ncurses.h>

#include "grille.h"
#include "snake.h"

int main(int argc, char const *argv[])
{
    srand(time(NULL));

    initscr();
    curs_set(0);
    noecho();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);

    int w, h;

    if (argc == 1)
    {
        w = 30;
        h = 15;
    }
    else
    {
        w = atoi(argv[1]);
        h = atoi(argv[2]);
    }

    init_grille(w, h);
    init_snake(5, w, h);

    add_apple();

    game(w, h);

    remove_grille();
    remove_snake();

    endwin();

    return 0;
}
