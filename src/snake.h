#ifndef SNAKE_H
#define SNAKE_H

#include <stdlib.h>

#include "coord.h"

typedef struct snake_s
{
    dot d;

    struct snake_s *next;
    struct snake_s *prev;
} snake_c;

typedef struct
{
    int last_key;
    int apple;
    dot dir;
    snake_c *c;
} snake_h;

extern void init_snake(size_t size, size_t w, size_t h);

extern int game(int w, int h);

extern void display();

extern void remove_snake();

#endif /* *SNAKE_H */
