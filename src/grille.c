#include <stdlib.h>
#include <ncurses.h>
#include <string.h>

#include "grille.h"
#include "coord.h"

static enum Case *grille;
static size_t width, height;
static WINDOW *win;

static char get_char_case(int x, int y);

void init_grille(size_t w, size_t h)
{
    width = w;
    height = h;
    grille = (Case *)malloc(sizeof(int) * width * height);

    for (size_t i = 0; i < w * h; i++)
    {
        grille[i] = None;
    }

    win = subwin(stdscr, height + 2, width + 2, 0, 0);
    box(win, ACS_VLINE, ACS_HLINE);
}

Case get_case(size_t x, size_t y)
{
    return grille[(y * width) + x];
}

void set_case(size_t x, size_t y, Case c)
{
    grille[(y * width) + x] = c;
    mvwaddch(win, y + 1, x + 1, get_char_case(x, y));
    wrefresh(win);
}

void set_case_dot(dot d, Case c)
{
    grille[(d.y * width) + d.x] = c;
    mvwaddch(win, d.y + 1, d.x + 1, get_char_case(d.x, d.y));
    wrefresh(win);
}

void add_apple()
{
    int x, y, d;
    do
    {
        d = 1;
        x = random() % width;
        y = random() % height;
        if (get_case(x, y) == None)
        {
            d = 0;
        }
    } while (d);
    set_case(x, y, Apple);
}

void remove_grille()
{
    delwin(win);
    free(grille);
}

void display()
{
    for (size_t y = 1; y <= height; y++)
    {
        for (size_t x = 1; x <= width; x++)
        {
            mvwaddch(win, y, x, get_char_case(x - 1, y - 1));
        }
    }
}

size_t get_width()
{
    return width;
}

size_t get_height()
{
    return height;
}

static char get_char_case(int x, int y)
{
    Case c = get_case(x, y);
    if (c == None)
    {
        return ' ';
    }
    else if (c == Snake)
    {
        return '#';
    }
    else if (c == SnakeH)
    {
        return '0';
    }
    else if (c == Apple)
    {
        return 'O';
    }
}